/*
 * Christian Flores Meléndez
 * 
 * 
 */
public class Calc 
{
    Map<String,Operation> operations;
    public Calc()
    {
        operations=new HashMap<String,Operation>();
        register("+",new Sum());
        register("-",new Substraction());
    }
    public void register(String operator,Operation operation)
    {
        operations.put(operator,operation);
    }
    public double operate(String operator,double a,double b)
    {
        Operation operation = operations.get(operator);
        return operation.operate(a,b);
    }

}
abstract class Operation
{
    public abstract double operate(double a,double b);
}
class Multiplicacion extends Operation
{
    public double operate(double a,double b)
    {
        return a*b;
    }
}

class Division extends Operation
{
    public double operate(double a,double b)
    {
        return a/b;
    }
}
class Sum extends Operation
{
    public double operate(double a,double b)
    {
        return a+b;
    }
}
class Substraction extends Operation
{
    public double operate(double a,double b)
    {
        return a-b;
    }
}

class Arroba extends Operation{
    public double operate (int a, int b){
        return (b*(a-2))/(a+b);
    }
}


public class Client
{

    public static void main (String  [] args){
    Calc calc = new Calc();
    calc.register("*",new Multiplicacion());
    calc.register("/",new Division());
    calc.register("@",new Arroba());
    double result = calc.operate("*",5,9);;
    System.out.println(result);
    }
}



